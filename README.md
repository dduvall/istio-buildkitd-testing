# Istio Buildkitd Testing

Sets up a minikube profile for debugging [interactions between Istio and CNI
iptables rules](https://phabricator.wikimedia.org/T330433).

You're need the following installed:

 - [istioctl](https://istio.io/latest/docs/setup/install/istioctl/)
 - [minikube](https://minikube.sigs.k8s.io/docs/start/)
 - [helm](https://helm.sh/docs/intro/install/)

Then just do:

```
make setup
```
