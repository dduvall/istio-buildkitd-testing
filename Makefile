BUILDKIT_CHART := https://gitlab.wikimedia.org/repos/releng/buildkit/-/jobs/artifacts/wmf/v0.11/raw/wmf/charts/buildkitd.tgz?job=package-and-publish-helm-chart

.PHONY: setup clean

setup:
	minikube start -p istio-buildkitd
	istioctl install -y --set meshConfig.accessLogFile=/dev/stdout
	kubectl patch namespace default -p '{"metadata": {"labels": {"istio-injection": "enabled"}}}'
	helm upgrade --install -f buildkitd-values.yaml buildkitd $(BUILDKIT_CHART)
	kubectl apply -f debug-pod.yaml

test:
	kubectl exec replicaset/debug-buildkit -- sh build.sh

trace:
	kubectl exec deployment/buildkitd -c buildkitd -- iptables-legacy -t raw -I PREROUTING -p tcp --destination apt1001.wikimedia.org --dport 80 -j TRACE

clean:
	minikube delete -p istio-buildkitd
